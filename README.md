# contrib/iTasks-SDK

This is a fork of the [iTasks][] project to enable external users to
contribute.

## Workflow

The general workflow is:

- You have a `master` branch up to date with the [official][iTasks] `master`
- You create feature branches in the contrib repository (this one)
- Merge requests go to the [official][iTasks] repository
- The contrib `master` branch has no use except showing you this readme

## Setting things up

On linux/unix systems:

```bash
git clone git@gitlab.science.ru.nl:clean-and-itasks/iTasks-SDK
cd iTasks-SDK
git remote add contrib git@gitlab.science.ru.nl:clean-and-itasks/contrib/iTasks-SDK
```

Alternatively with HTTPS:

```bash
git clone https://gitlab.science.ru.nl/clean-and-itasks/iTasks-SDK
cd iTasks-SDK
git remote add contrib https://gitlab.science.ru.nl/clean-and-itasks/contrib/iTasks-SDK
```

## Checking out a new feature branch

We want to start a new feature branch from the current master:

```bash
git checkout master
git pull
git checkout -b my-new-feature
# make your changes and commit
git push --set-upstream contrib my-new-feature
```

GitLab will respond with an URL that you can use to create a merge request. Be
sure to select the official repository as the target!

## More documentation

- See the [overview][] of the iTasks system and the codebase
- We use the [Platform standards][] for code style
- There is an explanation of the [documentation format][]

[iTasks]: https://gitlab.science.ru.nl/clean-and-itasks/iTasks-SDK
[overview]: https://gitlab.science.ru.nl/clean-and-itasks/iTasks-SDK/blob/master/Documentation/OVERVIEW.md
[Platform standards]: https://gitlab.science.ru.nl/clean-and-itasks/clean-platform/blob/master/doc/STANDARDS.md
[documentation format]: https://gitlab.science.ru.nl/clean-and-itasks/clean-platform/blob/master/doc/DOCUMENTATION.md
